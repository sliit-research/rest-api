'use strict';

const express = require('express');
const bodyParser= require('body-parser');
const app = express();
let http = require('http');
let server = http.createServer(app);
let io = require('socket.io')(server);
let mongoose = require('mongoose');
let config = require('./config.json');
let cors = require('cors');

let userRoutes = require('./routes/users')

let PORT = config.port;

let DB_PORT = config.db.port;
let DB_HOST_NAME = config.db.host;
let DB_NAME = config.db.database;
let DB_USERNAME = config.db.username;
let DB_PASSWORD = config.db.password;

mongoose.connect('mongodb://' + DB_USERNAME + ':' + DB_PASSWORD + '@' + DB_HOST_NAME + ':'  + DB_PORT + '/' + DB_NAME);

app.use(bodyParser.json());

app.use(cors({ origin: ['http://localhost:4200', /\.treespirit\.xyz$/] }));

app.get('/', function (req, res) {
    res.send('Welcome to TreeSpirit REST API');
});

app.use('/auth', userRoutes);

let sock = io.on("connection",(socket)=> {
    socket.emit('message',{data: 'test 1' });
});

app.get('/socket', function (req, res) {
    sock.emit('alert',{data: 'TEST' });
    res.send('Successfully Broadcasted!');
});

server.listen(PORT);